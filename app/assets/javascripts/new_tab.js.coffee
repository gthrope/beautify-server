# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

window.initBrowsePage = ->
  configControl $(control) for control in $(".controls")

configControl = (control) ->

    $buttons = control.find(".button")
    $buttons.click ->
      _gaq.push ["_trackEvent", "NewTab", $(this).attr("id")];

    $shareButtons = control.find("#shareBtns a")
    $shareButtons.click ->
      _gaq.push ["_trackEvent", "NewTabShare", $(this).attr("id")];

    $like = control.find("#Like")
    $like.click ->
      control.find("#likeform").submit()
      $(this).addClass "clicked" # not toggle because don"t have unlike...

      FB.getLoginStatus (response) ->
        if response.status is "connected"
          FB.api "me/og.likes", "post",
            object: control.attr "image_url"
          , (response) ->

    $dislike = control.find("#Dislike")
    $dislike.click ->
      $("#dislikeform").submit()

    $share = control.find("#Share")
    $share.click ->
      control.find("#shareBtns").toggleClass "clicked"

instrumentImg = ->
  $mainImg = $(".mainImg")
  if $mainImg.length and window.location.pathname is "/chromebrowse"
      _gaq.push(["_trackEvent", "ImageLoad", "Requested"])
      $mainImg.load ->
        _gaq.push(["_trackEvent", "ImageLoad", "Loaded"])

$(document).ready ->
  window.initBrowsePage()
  instrumentImg()