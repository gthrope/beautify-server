$(document).ready ->
  $categories = $("#set_cats .category")
  if $categories.length
    $categories.click catClicked
  $sources = $("#set_cats .source")
  if $sources.length
    $sources.click srcClicked

catClicked = ->
  $cat = $(this)
  $cat.toggleClass "clicked"
  id = $cat.attr("cat_id")
  $input = $("#saveCatsForm #cat_" + id)
  $input.val(if $input.val() then "" else id)

srcClicked = ->
  $src = $(this)
  $src.toggleClass "clicked"
  id = $src.attr("src_id")
  $input = $("#saveCatsForm #src_" + id)
  $input.val(if $input.val() then "" else id)
