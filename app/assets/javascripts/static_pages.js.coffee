# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

# Sign in & sign up

$(document).ready ->
  $fbButton = $(".fbButton")
  if $fbButton.length
    $fbButton.click fbClick

fbClick = ->
  FB.login ((response) ->
    if response.authResponse
      getUserInfoAndPost()),
  scope: "email,publish_actions"

getUserInfoAndPost = ->
  FB.api "/me?fields=name,email,third_party_id", (response) ->
    $form = $("form")
    $form.find("#emailField").val response.email
    $form.find("#passwordField").val response.third_party_id
    if $form.attr("id") is "userForm"
      $form.find("#nameField").val response.name
      $form.find("#confirmField").val response.third_party_id
    $form.submit()

# Devices page

window.installExtension = (nextPath) ->
  _gaq.push(['_trackEvent', 'Devices', 'InstallClick'])
  if window.chrome
    chrome.webstore.install '', ->
      _gaq.push(['_trackEvent', 'Devices', 'InstallSuccess', 'Chrome'])
      setTimeout (->
        window.location.href = nextPath
      ), 100
  else if navigator.userAgent.toLowerCase().indexOf('firefox') > -1
    window.open "https://addons.mozilla.org/en-US/firefox/addon/beautify/"
    _gaq.push(['_trackEvent', 'Devices', 'InstallSuccess', 'Firefox'])
    setTimeout (->
      window.location.href = nextPath
    ), 100
  else
    _gaq.push(['_trackEvent', 'Devices', 'Unsupported', navigator.userAgent])
    setTimeout (->
      window.location.href = '/unsupported'
    ), 100
