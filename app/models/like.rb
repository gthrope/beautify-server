class Like < ActiveRecord::Base
  attr_accessible :image_id

  belongs_to :user
  belongs_to :image

  validates :image_id, presence: true
  validates :user_id, presence: true

  default_scope order: 'likes.created_at DESC'
end
