class Artist < ActiveRecord::Base
  attr_accessible :name, :info, :birth_year, :death_year, :link
  has_many :images, dependent: :destroy
  validates :name, presence: true
  validates :info, presence: true
  validates :birth_year, presence: true
  validates :link, presence: true
end
