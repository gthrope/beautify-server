class Category < ActiveRecord::Base
  attr_accessible :name, :description, :link, :thumbnail
  has_many :images, dependent: :destroy
  validates :name, presence: true
  validates :description, presence: true
  validates :link, presence: true
  validates :thumbnail, presence: true
end
