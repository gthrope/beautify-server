class Image < ActiveRecord::Base
  attr_accessible :name, :artist_id, :category_id, :source_id, :medium_id, :image_url, :buy_url, :width, :height, :price, :like_count, :view_count

  has_many :likes, dependent: :destroy
  belongs_to :artist
  belongs_to :category
  belongs_to :source

  validates :name, presence: true
  validates :artist_id, presence: true
  validates :category_id, presence: true
  validates :source_id, presence: true
  validates :medium_id, presence: true
  validates :image_url, presence: true
  validates :buy_url, presence: true
  validates :width, presence: true
  validates :height, presence: true
  validates :price, presence: true

  # override as_json so mobile clients get appropriate data
  def as_json(options)
  { 
     name: name,
     artist: artist.name,
     image_url: image_url,
     buy_url: buy_url
  }
  end

end
