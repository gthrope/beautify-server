class Source < ActiveRecord::Base
  attr_accessible :description, :link, :name, :thumbnail
  has_many :images, dependent: :destroy
  validates :name, presence: true
  validates :description, presence: true
  validates :link, presence: true
  validates :thumbnail, presence: true
end
