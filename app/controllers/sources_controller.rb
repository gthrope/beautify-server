class SourcesController < ApplicationController
  def show
    @source = Source.find(params[:id])
    render layout: "left_pane"
  end

  def index
    @sources = Source.all
    render layout: "left_pane"
  end
end
