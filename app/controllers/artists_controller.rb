class ArtistsController < ApplicationController
  def show
    @artist = Artist.find(params[:id])
    render layout: "left_pane"
  end

  def index
    @artists = Artist.all
    render layout: "left_pane"
  end
end
