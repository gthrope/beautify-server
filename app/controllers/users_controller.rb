class UsersController < ApplicationController
  before_filter :signed_in_user, only: [:likes, :settings, :update]

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      sign_in @user
      flash[:success] = "Welcome to Beautify!"
      UserMailer.welcome_email(@user).deliver
      redirect_to setcategories_path # render 'static_pages/set_category_prefs' ?
    else
      render 'new'
    end
  end

  def likes
    @images = current_user.likes.map { |like| like.image }
    respond_to do |format|
      format.html {
        render layout: "left_pane"
      }
      format.json {
        render json: @images
      }
    end
  end

  def settings
    @user = current_user
  end

  def update
    if current_user.update_attributes(params[:user])
      flash[:success] = "Profile updated"
      redirect_to featured_path
    else
      @user = current_user
      render 'settings'
    end
  end

  private
 
    def signed_in_user
      redirect_to signin_url, notice: "Please sign in." unless signed_in?
    end

end
