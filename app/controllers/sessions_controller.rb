class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by_email(params[:session][:email].downcase);
    if(user && user.authenticate(params[:session][:password]))
      respond_to do |format|
        sign_in user
        # eliminate redirect?        
        format.html { redirect_to featured_path }
        format.json { render text: user.remember_token }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = 'Invalid email/password combination'
        format.html { render new_session_path }
        format.json { render status: :unauthorized }
        format.js
      end
    end
  end

  def destroy
    sign_out
    redirect_to root_path
  end

end
