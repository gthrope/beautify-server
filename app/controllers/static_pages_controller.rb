class StaticPagesController < ApplicationController
  include NewTabHelper

  def home
    if signed_in?
      redirect_to featured_path
    end
  end

  def sign_up
    # create new User variable for users signing up via Facebook
    @user = User.new
  end

  def featured
    artistId = 309
    if Date.today.yday() % 2 == 1
       artistId = 311
    end
    @artist = Artist.find(artistId)
    render layout: "left_pane"
  end

  def top
    @images = Image.order("like_count DESC").first(15)
    render layout: "left_pane"
  end

  # new controller?

  def set_category_prefs

    # parse existing category prefs
    if current_user.category_prefs.nil?
      @category_settings = []
    else
      @category_settings = current_user.category_prefs.split(",").map(&:to_i)
    end

    # parse existing source prefs
    if current_user.source_prefs.nil?
      @source_settings = []
    else
      @source_settings = current_user.source_prefs.split(",").map(&:to_i)
    end

  end

  #TODO: edge case where we add new category between set and save
  def save_category_prefs

    # store user inputs
    current_user.category_prefs = params[:catSets].select{ |id| !id.empty? }.join(",")
    current_user.source_prefs = params[:srcSets].select{ |id| !id.empty? }.join(",")
    current_user.save(validate: false)

    # set next_image; used for pre-caching
    set_next_image

    flash[:success] = "Categories saved"
    redirect_to mylikes_path
  end
end
