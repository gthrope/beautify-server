class NewTabController < ApplicationController
  include NewTabHelper
  before_filter :signed_in_user, only: [:like, :dislike]

  def show
    @image = get_next_image
    increment_view_count(@image)
    set_next_image

    if signed_in?
      # instrumentation
      current_user.last_image = @image.id
      current_user.last_image_time = DateTime.now
      current_user.save(validate: false)
    end

    respond_to do |format|
      format.html { render :layout => false }
      format.json { render :json => @image }
    end
  end

  def like
    @like = current_user.likes.build(params[:like])
    @image = @like.image
    @image.like_count += 1
    respond_to do |format|
      if @like.save && @image.save
        format.js
      else
        #TODO: log error
      end
    end
  end

  def dislike
    #@dislike = current_user.dislikes.build(params[:dislike])
    @image = get_next_image
    increment_view_count(@image)
    set_next_image
    respond_to do |format|
      format.js
    end
  end

  private

    def signed_in_user
      if !signed_in?
        respond_to do |format|
          format.js { render action: 'redirect' }
        end
      end
    end

    def increment_view_count(image)
      if image.view_count.nil?
        image.view_count = 0
      end
      image.view_count += 1
      image.save(validate: false)
    end

end
