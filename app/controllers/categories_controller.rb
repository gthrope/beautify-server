class CategoriesController < ApplicationController
  def show
    @category = Category.find(params[:id])
    render layout: "left_pane"
  end

  def index
    @categories = Category.all
    render layout: "left_pane"
  end
end
