module NewTabHelper

  # get next image to show, checking cookie first
  def get_next_image

    image_id = cookies[:next_image]
    if(image_id.nil?) # no cookie
      image = generate_next_image
    else
      image = Image.find(image_id)
    end

    # return image
    image
  end

  # set next image to show; used for pre-caching
  def set_next_image
    next_image = generate_next_image
    cookies.permanent[:next_image] = next_image.id
  end

  private

    # get image satisfying category and source preferences
    def generate_next_image

      # not signed in, never set prefs, or set prefs to everything
      if !signed_in? || current_user.category_prefs.nil? || (current_user.category_prefs.empty? && (current_user.source_prefs.nil? || current_user.source_prefs.empty?))
        image = Image.first(:offset => rand(Image.count)) # random image
      else

        # parse category prefs
        category_prefs = current_user.category_prefs.split(",").map(&:to_i)

        # parse source prefs; could be nil for older users
        source_prefs = []
        if(!current_user.source_prefs.nil?)
          source_prefs = current_user.source_prefs.split(",").map(&:to_i)
        end

        # get image using prefs
        imagePool = Image.where("category_id IN (?) OR source_id IN (?)", category_prefs, source_prefs)
        image = imagePool.first(:offset => rand(imagePool.count))

      end

      # return image
      image
    end

end