module SessionsHelper
  include NewTabHelper

  def sign_in(user)
    cookies.permanent[:remember_token] = user.remember_token
    # set next image cookie, for pre-caching; this will be overwritten if user sets categories
    set_next_image
    self.current_user = user
  end

  def signed_in?
    !self.current_user.nil?
  end

  def current_user=(user)
    @current_user = user
  end

  def current_user
    @current_user ||= User.find_by_remember_token(cookies[:remember_token])
  end

  def current_user?(user)
    user == current_user
  end

  def sign_out
    self.current_user = nil
    cookies.delete(:remember_token)
  end

end
