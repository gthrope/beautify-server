module ApplicationHelper

  def full_title(page_title)
    base_title = "Beautify"
    if page_title.empty?
      base_title
    else
      page_title
    end
  end

  def image_display_name(image)
    "#{image.artist.name}, \"#{image.name}\""
  end

end
