RailsServer::Application.routes.draw do

  # clean this up. move update out of resources, stop using id. consolidate new
  resources :users, only: [:new, :create, :update]
  # also allow Posts to new user page for users signing up via Facebook

  match 'users/new' => 'users#new', via: :post
  match 'mylikes' => 'users#likes'
  match 'settings' => 'users#settings'

  #TODO: clean this up
  resources :sessions, only: [:new, :create, :destroy]
  match '/signin', to: 'sessions#new'
  match '/signout', to: 'sessions#destroy', via: :delete

  resources :images, only: [:show]
  resources :artists, only: [:show, :index]
  resources :categories, only: [:show, :index]
  resources :sources, only: [:show, :index]

  match '/chromebrowse', to: 'new_tab#show'
  match '/likes', to: 'new_tab#like', via: :post
  match '/dislikes', to: 'new_tab#dislike', via: :post

  match '/signup', to: 'static_pages#sign_up'
  match '/devices', to: 'static_pages#devices'
  match '/help', to: 'static_pages#help'
  match '/about', to: 'static_pages#about'
  match '/contact', to: 'static_pages#contact'
  match '/unsupported', to: 'static_pages#unsupported'
  match '/setcategories', to: 'static_pages#set_category_prefs'
  match '/savecategories', to: 'static_pages#save_category_prefs'
  match '/feedback', to: 'static_pages#feedback'
  match '/terms', to: 'static_pages#terms'
  match '/featured', to: 'static_pages#featured'
  match '/top', to: 'static_pages#top'
  root to: 'static_pages#home'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
