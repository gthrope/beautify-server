# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130731044145) do

  create_table "artists", :force => true do |t|
    t.string   "name"
    t.text     "info"
    t.integer  "birth_year"
    t.integer  "death_year"
    t.string   "link"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "thumbnail"
    t.string   "link"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "images", :force => true do |t|
    t.string   "name"
    t.integer  "artist_id"
    t.integer  "category_id"
    t.integer  "medium_id"
    t.string   "image_url"
    t.string   "buy_url"
    t.string   "width"
    t.string   "height"
    t.decimal  "price"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "like_count",  :default => 0
    t.integer  "source_id"
    t.integer  "view_count"
  end

  add_index "images", ["category_id"], :name => "index_images_on_category_id"
  add_index "images", ["like_count"], :name => "index_images_on_like_count"

  create_table "likes", :force => true do |t|
    t.integer  "user_id"
    t.integer  "image_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "likes", ["user_id", "created_at"], :name => "index_likes_on_user_id_and_created_at"

  create_table "sources", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "link"
    t.string   "thumbnail"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "password_digest"
    t.string   "remember_token"
    t.integer  "last_image"
    t.datetime "last_image_time"
    t.string   "source_prefs"
    t.string   "category_prefs"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["remember_token"], :name => "index_users_on_remember_token"

  create_table "waitlist_entries", :force => true do |t|
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
