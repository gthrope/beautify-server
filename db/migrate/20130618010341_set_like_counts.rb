class SetLikeCounts < ActiveRecord::Migration
  def up
    Like.all.each do |like|
      like.image.like_count = like.image.like_count + 1
      like.image.save
    end
  end

  def down
  end
end
