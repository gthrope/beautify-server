class AddViewCountToImages < ActiveRecord::Migration
  def change
    add_column :images, :view_count, :integer
  end
end
