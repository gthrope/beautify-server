class AddSourcePrefsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :source_prefs, :string
  end
end
