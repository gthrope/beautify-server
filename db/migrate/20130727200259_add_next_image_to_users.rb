class AddNextImageToUsers < ActiveRecord::Migration
  def change
    add_column :users, :next_image, :int
  end
end
