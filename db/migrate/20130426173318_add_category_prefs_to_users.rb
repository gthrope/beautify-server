class AddCategoryPrefsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :category_prefs, :string
  end
end
