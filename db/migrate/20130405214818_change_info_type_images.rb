class ChangeInfoTypeImages < ActiveRecord::Migration
  def up
    change_column :images, :info, :text
  end

  def down
    change_column :images, :info, :string
  end
end
