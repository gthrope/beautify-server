class RecreateImages < ActiveRecord::Migration
  def up
    create_table :images do |t|
      t.string :name
      t.integer :artist_id
      t.integer :category_id
      t.integer :medium_id
      t.string :image_path
      t.string :affiliate_url_param
      t.string :width
      t.string :height
      t.decimal :price

      t.timestamps
    end
    add_index :images, :category_id
  end

  def down
    drop_table :images
  end
end
