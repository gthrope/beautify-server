class AddIndexToImagesLikeCount < ActiveRecord::Migration
  def change
    add_index :images, :like_count, :order => {:like_count => :desc}
  end
end
