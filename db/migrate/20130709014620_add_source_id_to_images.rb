class AddSourceIdToImages < ActiveRecord::Migration
  def change
    add_column :images, :source_id, :integer
  end
end
