class AddLastImageToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_image, :integer
    add_column :users, :last_image_time, :datetime
  end
end
