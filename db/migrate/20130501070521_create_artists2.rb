class CreateArtists2 < ActiveRecord::Migration
  def up
    create_table :artists do |t|
      t.string :name
      t.text :info
      t.integer :birth_year
      t.integer :death_year
      t.string :link

      t.timestamps
    end
  end

  def down
    drop_table :artists
  end
end
