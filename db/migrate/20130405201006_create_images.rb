class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :name
      t.integer :artist_id
      t.integer :source_id
      t.string :year
      t.string :info
      t.string :imageUrl
      t.string :buyUrl

      t.timestamps
    end
    add_index :images, :artist_id
    add_index :images, :source_id
  end
end
