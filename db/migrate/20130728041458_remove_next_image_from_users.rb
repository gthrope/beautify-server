class RemoveNextImageFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :next_image
  end

  def down
    add_column :users, :next_image, :int
  end
end
