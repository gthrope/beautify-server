class FixColumnNames < ActiveRecord::Migration
  def up
    rename_column :images, :image_path, :image_url
    rename_column :images, :affiliate_url_param, :buy_url
  end

  def down
  end
end
