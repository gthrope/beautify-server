class CreateSources < ActiveRecord::Migration
  def change
    create_table :sources do |t|
      t.string :name
      t.string :description
      t.string :link
      t.string :thumbnail

      t.timestamps
    end
  end
end
