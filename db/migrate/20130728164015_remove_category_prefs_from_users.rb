class RemoveCategoryPrefsFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :category_prefs
    rename_column :users, :category_prefs2, :category_prefs
  end

  def down

  end
end
