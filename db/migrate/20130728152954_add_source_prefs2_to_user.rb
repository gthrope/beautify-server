class AddSourcePrefs2ToUser < ActiveRecord::Migration
  def change
    add_column :users, :category_prefs2, :string
    User.all.each do |u|
      if !u.category_prefs.nil?
        category_prefs2 = []
        u.category_prefs.split("").each_with_index do |c, i|
          if c == "1"
            category_prefs2.push Category.find(i+1).id
          end
        end
        u.category_prefs2 = category_prefs2.join(",")
        u.save(validate: false)
      end
    end
  end
end
