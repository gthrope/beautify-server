require 'csv'

desc "Import images from csv file"
task :import_images => [:environment] do

  file = "db/images.csv"

  CSV.foreach(file) do |row|
    Image.create ({
      name: row[4],
      artist_id: row[7],
      category_id: row[8],
      medium_id: row[2],
      image_path: row[3],
      affiliate_url_param: row[5],
      width: row[0],
      height: row[1],
      price: row[6],
    })
  end

end