require 'csv'

desc "Import artists from csv file"
task :import_artists => [:environment] do

  file = "db/artists.csv"

  CSV.foreach(file) do |row|
    Artist.create ({
      name: row[0],
      info: row[1],
      birth_year: row[2],
      death_year: row[3],
      link: row[4]
    })
  end

end