require 'csv'

desc "Import categories from csv file"
task :import_categories => [:environment] do

  file = "db/categories.csv"

  CSV.foreach(file) do |row|
    Category.create ({
      name: row[0],
      description: row[1],
      link: row[2],
      thumbnail: row[3]
    })
  end

end